package com.example;

import java.util.Arrays;

public class StringConvertion {
    String firstString;

    public StringConvertion(String firstString) {
        this.firstString = new String(firstString);
    }
    public void convertLowerAndUpperCase(){
       char [] arr=firstString.toCharArray();
        int firstHalf=arr.length/2;
        for (int iterate = 0; iterate < arr.length; iterate++) {
            if(iterate < firstHalf){
                if(arr[iterate]>='a' && arr[iterate] <= 'z')
                arr[iterate]= (char) (arr[iterate]-32);
            }else{
                if(arr[iterate]>='A' && arr[iterate] <= 'Z')
                arr[iterate]= (char) (arr[iterate]+32);
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}
